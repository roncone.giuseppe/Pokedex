// -----------------------------WRAPPER---------------------------
let cardsWrapper = document.querySelector('#cardsWrapper')

// ---------------------------BUTTONS--------------------------
let showPokemonBtn = document.querySelector('#showPokemonBtn')

let addPokemonBtn  = document.querySelector('#addPokemonBtn')

let removePokemonBtn = document.querySelector('#removePokemonBtn')

let searchPokemonBtn = document.querySelector('#searchPokemonBtn')

// ------------------------------INPUT---------------------------

let nameInput = document.querySelector('#nameInput');

let codeInput = document.querySelector('#codeInput')


// -----------------------INIZIO POKEDEX---------------------

const pokedex = {

    pokemons : [

        {name : 'Pikachu', code : 01},
        {name : 'Charizard', code : 06},
        {name : 'Bulbasaur', code : 02},
        {name : 'Squirtle', code : 03},
    ],

   showPokemon :  function(array) {

    cardsWrapper.innerHTML = '';


        array.forEach ((pokemon) => {

        let div = document.createElement ('div');

        div.classList.add('col-12', 'd-flex', 'justify-content-center');

        div.innerHTML = `
        <div class="col-12 d-flex justify-content-between align-items-center py-3 px-3 card-custom">

        <p class="bold">${pokemon.name}</p>

        <p class="bold">${pokemon.code}</p>

        <p class="bold"> <i> <img class="pokeball" src="./769px-Pokebola-pokeball-png-0.png" alt=""></i> </p>
        
        </div>
        `

        cardsWrapper.appendChild(div);

     })

    },

        addPokemon : function(newName, newCode) {

            if(newName != '' && newCode != '') {

                this.pokemons.push({name : newName, code : newCode});

                this.showPokemon(this.pokemons);

                nameInput.value = ''
                codeInput.value = ''
             

            } else {

                alert('Attenzione devi scansionare il tuo Pokémon!')


            }

        },

        removePokemon : function (removedName) {

            let names = this.pokemons.map ((pokemon) => pokemon.name.toLowerCase());

            let index = names.indexOf (removedName.toLowerCase());

            if (index > -1) {

                this.pokemons.splice ( index ,1);

                this.showPokemon(this.pokemons) ;

            } else {

                alert('Pokémon non trovato');

            }

        },

        searchPokemon : function(searchedName) {

                let filtered = this.pokemons.filter( (pokemon) => searchedName == pokemon.name);

                

                if(filtered.length > 0) {

                    this.showPokemon(filtered)

                } else {


                    // confirm = false

                    alert('Attenzione Pokémon non trovato!')

                    showPokemonBtn.innerHTML = 'Accendi Pokedex'
              
                }

               


        }





}




// -------------FUNZIONE TASTO ACCENDI SPEGNI----------------

let confirm = false 

showPokemonBtn.addEventListener ('click', ()=>{

    if (confirm == false){

        pokedex.showPokemon(pokedex.pokemons);

         confirm = true;

         showPokemonBtn.innerHTML = 'Spegni Pokedex';

    } else {

    cardsWrapper.innerHTML = '';
    confirm = false
    showPokemonBtn.innerHTML = 'Accendi Pokedex';

}
})

// -----------------FUNZIONE TASTO AGGIUNGI--------------------

addPokemonBtn.addEventListener('click', ()=>{   

        if(nameInput.value != '' && codeInput.value != '') {

        confirm = true

        pokedex.addPokemon(nameInput.value, codeInput.value);

        showPokemonBtn.innerHTML = 'Spegni Pokedex'; }

        else {

            alert('Attenzione devi scansionare il tuo Pokémon!')

            addPokemonBtn.innerHTML = 'Aggiungi un Pokemon'

        }

    }
)

// ----------------------FUNZIONE TASTO RIMUOVI------------------------

removePokemonBtn.addEventListener ('click', ()=> {

    confirm = true
    pokedex.removePokemon (nameInput.value);

    showPokemonBtn.innerHTML = 'Accendi Pokedex';

    nameInput.value = '';
})

// -------------------FUNZIONE TASTO CERCA---------------------

searchPokemonBtn.addEventListener ('click', ()=> {

    confirm = true;

    pokedex.searchPokemon(nameInput.value);

    showPokemonBtn.innerHTML = 'Accendi Pokedex';

    nameInput.value = '';


})